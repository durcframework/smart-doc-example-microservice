package com.power.shop.api.controller;


import com.power.shop.api.controller.vo.OrderVO;
import com.power.shop.commons.bean.Result;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 订单接口
 */
@RestController
@RequestMapping("order")
public class OrderController {

    /**
     * 获取订单信息
     * @param orderNo 订单id
     * @return
     */
    @GetMapping("get")
    public Result<OrderVO> getOrder(String orderNo) {
        return null;
    }

}

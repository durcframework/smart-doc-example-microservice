package com.power.shop.api.controller.vo;

import lombok.Data;

@Data
public class OrderVO {

    /**
     * 订单id
     * @mock 2021110022334
     */
    private String orderNo;
}

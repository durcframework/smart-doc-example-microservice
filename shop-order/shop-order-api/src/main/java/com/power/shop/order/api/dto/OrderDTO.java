package com.power.shop.order.api.dto;


import lombok.Data;

@Data
public class OrderDTO {
    /**
     * 订单编号
     */
    private String orderNo;
    /**
     * 订单价格
     */
    private Integer price;
}

package com.power.shop.order.api.service;


import com.power.shop.order.api.dto.OrderDTO;

public interface OrderService {

    String createOrder(OrderDTO orderDTO);

}

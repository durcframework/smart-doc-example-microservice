package com.power.shop.commons.bean;

import lombok.Data;

@Data
public class Result<T> implements java.io.Serializable {

    private static final long serialVersionUID = 266696159469987136L;

    /**
     * 返回的状态，0成功, 其他失败
     *
     * @mock 0
     */
    private int code = 0;


    /**
     * 失败情况下返回失败的原因
     */
    private String msg;


    /**
     * 实际返回的数据实体
     */
    private T data;


}
